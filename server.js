const express = require('express')
const mongoose = require('mongoose')
const app = express()

app.use(express.json())

app.use('/', require('./routes/api'))

const PORT = process.env.PORT || 5000

mongoose.connect('mongodb://localhost/practical_task_db', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
}).then(() =>{
    console.log('mongodb connected...')
    app.listen(PORT, () => console.log(`server running on:${PORT}`))
})
