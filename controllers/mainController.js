const Data = require('../models/model')

exports.postData = async (req, res) => {
    try {
        const {
            name,
            profession,
            address
        } = req.body;
        if(!name || !profession || !address) {
            return res.status(400).send({ msg: 'All fields are required' })
        }

        const newData = new Data({
            name,
            profession,
            address
        })

        const data = await newData.save()
        return res.status(200).json({ data })
    } catch (err) {
        console.log(err)
        return res.status(500).json({ msg: 'internal server error' })
    }
}

exports.getData = async (req, res) => {
    try {
        const data = await Data.find()
        return res.status(200).json({ data })
    } catch (err) {
        console.log(err)
        return res.status(500).json({ msg: 'internal server error' })
    }
}

exports.updateData = async (req, res) => {
    try {
        const id = req.params.id
        const {
            name,
            profession,
            address
        } = req.body;

        const data = await Data.findById(id)
        if (!data) {
            return res.status(400).json({
                msg: 'no data found'
            })
        }

        if (name) data.name = name
        if (profession) data.profession = profession
        if (address) data.address = address

        await data.save()
        return res.status(200).json({ data, msg: 'data updated' })
    } catch (err) {
        console.log(err)
        return res.status(500).json({ msg: 'internal server error' })
    }
}

exports.deleteData = async (req, res) => {
    try {
        const id = req.params.id

        const data = await Data.findById(id)
        if(!data){
           return res.status(400).json({
               msg: 'no data found'
           })
        }

        await data.remove()
        return res.status(200).json({
            msg: 'data deleted'
        })
    } catch (err) {
        console.log(err)
        return res.status(500).json({
            msg: 'internal server error'
        })
    }
}