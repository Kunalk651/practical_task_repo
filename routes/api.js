const express = require('express');
const mainController = require('../controllers/mainController');
const router = express.Router()

router.get('/get-data', mainController.getData)

router.post('/post-data', mainController.postData)

router.post('/update-data/:id', mainController.updateData)

router.post('/delete-data/:id', mainController.deleteData)

module.exports = router;