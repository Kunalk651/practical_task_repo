const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    name: {
        type: String
    },
    profession: {
        type: String
    },
    address: {
        type: String
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('ModelSchema', modelSchema, 'data')